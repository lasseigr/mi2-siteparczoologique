import { updateAnimal } from './js-management-animals.js'

export default () => {
    const tbody = document.querySelector('#animaux #animaux-tbody')
    if (!tbody) return

    // récupération du tableau "animal" dans le fichier JSON
    $.getJSON('assets/js/source.json', (data) => {
        [...data["animals"]].forEach(animal => {
            const tr = document.createElement('tr')

            // Nom
            const nameData     = document.createElement('td')
            nameData.setAttribute('id', 'nameData')
            nameData.innerHTML = animal.name
            tr.appendChild(nameData)

            // Image
            const imageData = document.createElement('td')
            imageData.setAttribute('id', 'imageData')
            const img       = document.createElement('img')

            // TODO: image persistante
            // const reader = new FileReader()
            // reader.onload = (evt) => {
            //     if (animal.image) {
            //         img.setAttribute('src', evt.target.result)
            //     }
            // }
            // reader.readAsDataURL(animal.image)

            imageData.appendChild(img)
            tr.appendChild(imageData)

            // Description
            const descData     = document.createElement('td')
            descData.setAttribute('id', 'descData')
            descData.innerHTML = animal.desc
            tr.appendChild(descData)

            // Pays
            const paysData     = document.createElement('td')
            paysData.setAttribute('id', 'paysData')
            paysData.innerHTML = animal.pays
            tr.appendChild(paysData)

            // Agrandir
            const buttonData = document.createElement('td')
            const button     = document.createElement('button')
            button.innerHTML = 'Agrandir'
            button.classList.add('Button--green', 'user-button')
            buttonData.appendChild(button)
            tr.appendChild(buttonData)

            // Début
            const anchorData = document.createElement('td')
            const a          = document.createElement('a')
            a.innerHTML      = 'Début'
            a.setAttribute('href', '#animaux')
            anchorData.appendChild(a)
            tr.appendChild(anchorData)

            // Actions
            const buttonsData = document.createElement('td')
            const updateButton = document.createElement('button')
            const supprButton = document.createElement('button')

            updateButton.classList.add('Button--green', 'admin-button')
            updateButton.innerHTML = 'Modifier'
            supprButton.classList.add('Button--green', 'admin-button')
            supprButton.innerHTML = 'Supprimer'
            supprButton.addEventListener('click', () => {
                tr.remove()
                $.ajax({
                    url: "assets/js/removeAnimal.php",
                    type: "POST",
                    data: {animal: animal}
                })
            })
            updateButton.addEventListener('click', () => {
                updateAnimal(tr, animal)
            })
            buttonsData.appendChild(updateButton)
            buttonsData.appendChild(supprButton)
            tr.appendChild(buttonsData)

            tbody.appendChild(tr)
        })
    })
}
