export default () => {
    const modalTrigger = document.querySelector('#modal-trigger')
    const modalTarget  = document.querySelector('#modal-target')

    if (!modalTrigger || !modalTarget) return

    const formAccount = modalTarget.querySelector('#form-account')
    const closeSpan   = modalTarget.querySelector('.modal-close')

    formAccount.addEventListener('submit', (e) => {
        const lgn = formAccount.querySelector('#login').value
        const pwd = formAccount.querySelector('#password').value
        e.preventDefault()
        const obj = { login: lgn, password: pwd }

        $.ajax({
            url: 'login.php',
            type: 'POST',
            data: obj
        }).done(function (arg) {
            // si connexion réussie
            modalTarget.style.display = 'none'
            const adminButtons        = document.querySelectorAll('.admin-button')
            const userButtons        = document.querySelectorAll('.user-button')
            // si admin connecté
            if (arg.includes('Success') && lgn === 'admin') {
                // afficher tous les boutons
                [...adminButtons, ...userButtons].forEach(button => {
                    button.style.display = 'inline-block'
                })
                // si user connecté
            } else if(arg.includes('Success') && lgn === 'user') {
                // afficher seulement bouton de type user
                [...adminButtons, ...userButtons].forEach(adminButton => {
                    adminButton.style.display = 'none'
                });
                [...userButtons].forEach(userButton => {
                    userButton.style.display = 'inline-block'
                })
                // si personne connecté
            } else {
                // cacher tous les boutons
                [...adminButtons, ...userButtons].forEach(button => {
                    button.style.display = 'none'
                })
            }
        })
    })

    modalTrigger.addEventListener('click', () => {
        modalTarget.style.display = 'block'
    })

    closeSpan.addEventListener('click', () => {
        modalTarget.style.display = 'none'
    })

    window.addEventListener('click', (e) => {
        if (e.target === modalTarget) {
            modalTarget.style.display = 'none'
        }
    })
}
