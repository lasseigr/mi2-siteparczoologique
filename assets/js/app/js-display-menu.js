export default () => {
    const menuNav = document.querySelector('#menu-nav')
    if (!menuNav) return

    const menuItems = '{ "menu" : [' +
        '{ "name":"Accueil", "lien":"index.html" },' +
        '{ "name":"Espace perso", "lien":"perso.html", "items":["Connexion", "Mes informations", "Messagerie", "Historique"] },' +
        '{ "name":"Animaux", "lien":"animaux.html" },' +
        '{ "name":"Visite virtuelle audio", "lien":"virtuel_audio.html" },' +
        '{ "name":"Visite virtuelle vidéo", "lien":"virtuel_video.html" },' +
        '{ "name":"Contact", "lien":"contact.html" }' +
        ']}'

    const parseMenuItems = JSON.parse(menuItems)

    const currentPageName = window.location.pathname.split('/').pop()

    for (let i in parseMenuItems.menu) {
        const name  = parseMenuItems.menu[i].name
        const lien  = parseMenuItems.menu[i].lien
        const items = parseMenuItems.menu[i].items

        if (items) {
            const div = document.createElement('div')
            div.classList.add('Menu-with-submenu')

            const a = document.createElement('a')
            a.setAttribute('href', lien)
            if (lien.includes(currentPageName)) {
                a.classList.add('Menu-li', 'isActive')
            } else {
                a.classList.add('Menu-li')
            }

            a.innerHTML = name
            div.appendChild(a)

            const ul = document.createElement('ul')
            ul.classList.add('Menu-with-submenu__ul');
            [...items].forEach(item => {
                const li = document.createElement('li')
                li.classList.add('Menu-with-submenu__li')

                const aLi = document.createElement('a')
                aLi.classList.add('Menu-with-submenu__a')
                if (item.includes('Connexion')) {
                    aLi.setAttribute('id', 'modal-trigger')
                }
                aLi.innerHTML = item
                li.appendChild(aLi)

                ul.appendChild(li)
            })
            div.appendChild(ul)

            menuNav.appendChild(div)
        } else {
            const a = document.createElement('a')
            a.setAttribute('href', lien)
            if (lien.includes(currentPageName)) {
                a.classList.add('Menu-li', 'isActive')
            } else {
                a.classList.add('Menu-li')
            }
            a.innerHTML = name
            menuNav.appendChild(a)
        }
    }
}
