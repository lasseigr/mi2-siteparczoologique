const addAnimal = (form, tbody) => {
    const addName  = form.querySelector('#add-animal-name')
    const addImage = form.querySelector('#add-animal-image')
    const addDesc  = form.querySelector('#add-animal-desc')
    const addPays  = form.querySelector('#add-animal-pays')

    const newAnimal = { name:addName.value, image:null/*addImage.files[0]*/, desc:addDesc.value, pays:addPays.value }
    const formData = new FormData()
    formData.append("name", addName.value)
    formData.append("image", addImage.files[0])
    formData.append("desc", addDesc.value)
    formData.append("pays", addPays.value)
    // ajout du nouvel animal dans le JSON
    $.ajax({
        url: "assets/js/addAnimal.php",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        enctype: "multipart/form-data"
    })

    const tr = document.createElement('tr')

    // Nom
    const nameData     = document.createElement('td')
    nameData.setAttribute('id', 'nameData')
    nameData.innerHTML = addName.value
    tr.appendChild(nameData)

    // Image
    const imageData   = document.createElement('td')
    imageData.setAttribute('id', 'imageData')
    const img         = document.createElement('img')
    const [firstFile] = addImage.files
    img.setAttribute('src', URL.createObjectURL(firstFile))
    imageData.appendChild(img)
    tr.appendChild(imageData)

    // Description
    const descData     = document.createElement('td')
    descData.setAttribute('id', 'descData')
    descData.innerHTML = addDesc.value
    tr.appendChild(descData)

    // Pays
    const paysData     = document.createElement('td')
    paysData.setAttribute('id', 'paysData')
    paysData.innerHTML = addPays.value
    tr.appendChild(paysData)

    // Agrandir
    const buttonData = document.createElement('td')
    const button     = document.createElement('button')
    button.innerHTML = 'Agrandir'
    button.classList.add('Button--green', 'user-button')
    buttonData.appendChild(button)
    tr.appendChild(buttonData)

    // Début
    const anchorData = document.createElement('td')
    const a          = document.createElement('a')
    a.innerHTML      = 'Début'
    a.setAttribute('href', '#animaux')
    anchorData.appendChild(a)
    tr.appendChild(anchorData)

    // Actions
    const buttonsData = document.createElement('td')
    const updateButton = document.createElement('button')
    const supprButton = document.createElement('button')
    updateButton.classList.add('Button--green', 'admin-button')
    updateButton.innerHTML = 'Modifier'
    supprButton.classList.add('Button--green', 'admin-button')
    supprButton.innerHTML = 'Supprimer'
    supprButton.addEventListener('click', () => {
        tr.remove()
        $.ajax({
            url: "assets/js/removeAnimal.php",
            type: "POST",
            data: {animal: newAnimal}
        })
    })
    updateButton.addEventListener('click', () => {
        updateAnimal(tr, newAnimal)
    })
    buttonsData.appendChild(updateButton)
    buttonsData.appendChild(supprButton)
    tr.appendChild(buttonsData)

    tbody.appendChild(tr)
}

export const updateAnimal = (tr, animal) => {
    const container = document.querySelector('#form-update-animal')
    if (!container) return

    container.innerHTML = ''

    const formUpdate = document.createElement('form')
    formUpdate.setAttribute('id', 'form-update-animal')

    const updateName  = document.createElement('input')
    const updateImage = document.createElement('input')
    const updateDesc  = document.createElement('textarea')
    const updatePays  = document.createElement('input')

    const updateButton = document.createElement('input')
    updateButton.innerHTML = 'Modifier'
    updateButton.setAttribute('type', 'submit')
    updateButton.classList.add('Button--green', 'admin-button')

    const name = tr.querySelector('#nameData')
    const image = tr.querySelector('#imageData img')
    const desc = tr.querySelector('#descData')
    const pays = tr.querySelector('#paysData')

    updateName.setAttribute('value', name.innerHTML)
    updateName.setAttribute('type', 'text')
    updateName.setAttribute('required', 'required')

    updateImage.setAttribute('type', 'file')
    updateImage.setAttribute('accept', 'image/*')

    updateDesc.value = desc.innerHTML
    updateDesc.setAttribute('rows', 5)
    updateDesc.setAttribute('required', 'required')

    updatePays.setAttribute('value', pays.innerHTML)
    updatePays.setAttribute('type', 'text')
    updatePays.setAttribute('required', 'required')

    formUpdate.addEventListener('submit', (e) => {
        e.preventDefault()

        name.innerHTML = updateName.value
        let firstFile = null
        if (updateImage.files.length !== 0) {
            firstFile = updateImage.files[0] ?? null
            // image.setAttribute('src', URL.createObjectURL(firstFile))
        }
        desc.innerHTML = updateDesc.value
        pays.innerHTML = updatePays.value

        container.innerHTML = ''

        // mise à jour de l'animal "animal" dans le JSON
        $.ajax({
            url: "assets/js/updateAnimal.php",
            type: "POST",
            data: {animal: animal, newName: updateName.value, newImage: /*firstFile ??*/ null, newDesc: updateDesc.value, newPays: updatePays.value}
        })
    })
    formUpdate.appendChild(updateName)
    formUpdate.appendChild(updateImage)
    formUpdate.appendChild(updateDesc)
    formUpdate.appendChild(updatePays)
    formUpdate.appendChild(updateButton)

    container.appendChild(formUpdate)
}

export default () => {
    const form  = document.querySelector('#form-add-animal')
    const tbody = document.querySelector('#animaux #animaux-tbody')
    if (!form || !tbody) return

    form.addEventListener('submit', (e) => {
        e.preventDefault()
        addAnimal(form, tbody)
    })
}
