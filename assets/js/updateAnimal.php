<?php
$content = file_get_contents('source.json');
$json = json_decode($content, true);
foreach ($json["animals"] as $key => $object) {
    if ($object["name"] === $_POST["animal"]["name"]) {
        $json["animals"][$key]["name"] = $_POST["newName"] ?? '';
        $json["animals"][$key]["image"] = $_POST["newImage"] ?? '';
        $json["animals"][$key]["desc"] = $_POST["newDesc"] ?? '';
        $json["animals"][$key]["pays"] = $_POST["newPays"] ?? '';
    }
}
$str = json_encode($json);
file_put_contents('source.json', $str);
?>
