<?php
$content = file_get_contents('source.json');
$json = json_decode($content, true);
foreach ($json["animals"] as $key => $object) {
    if ($object["name"] === $_POST["animal"]["name"]) {
        unset($json["animals"][$key]);
    }
}
$str = json_encode($json);
file_put_contents('source.json', $str);
?>
