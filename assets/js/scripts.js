import { default as DisplayMenu } from './app/js-display-menu.js'
import { default as DisplayAnimals } from './app/js-display-animals.js'
import { default as ManagementAnimals } from './app/js-management-animals.js'
import { default as Account } from './app/js-account.js'

document.addEventListener('DOMContentLoaded', () => {
    DisplayAnimals()
    DisplayMenu()
    ManagementAnimals()
    Account()
})
